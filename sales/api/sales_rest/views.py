from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json


from common.json import ModelEncoder
from .models import Sale, Salesperson, Customer, AutomobileVO


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id"
    ]
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_vin"
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer",
        "automobile",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):

    if request.method == "GET":
        salespersons = Salesperson.objects.all()

        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder,
            safe=False)
    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    if request.method == "DELETE":
        count, _ = Salesperson.objects.get(employee_id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0 }
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):

    if request.method == "DELETE":
        try:
            count, _ = Customer.objects.get(id=pk).delete()
            return JsonResponse(
                {"deleted": count > 0 }
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "customer not found"},
                status=400
            )


@require_http_methods({"GET", "POST"})
def api_list_sales(request):

    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try: #get salesperson info
            id= content['salesperson']
            salesperson = Salesperson.objects.get(employee_id=id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "No Salesperson found"},
                status=400
            )

        try: #get customer info
            id= content["customer"]
            customer = Customer.objects.get(id=id)
            content["customer"] = customer
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "No customer found"},
                status=400
            )

        try: #get auto info
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(import_vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid Automobile vin"},
            status=400
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )


@require_http_methods({"DELETE"})
def api_delete_sale(request, pk):

    if request.method == "DELETE":
        try:
            count, _ = Sale.objects.get(id=pk).delete()
            return JsonResponse(
                {"message": count > 0}
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale not Found"},
                status=400
            )
