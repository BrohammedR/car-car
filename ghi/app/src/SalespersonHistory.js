import { useEffect, useState} from "react";
import { Link } from "react-router-dom";


function SalespersonHistory() {
    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([])

    //get salesperson data for dropdown menu
    const fetchSalesperson = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespersonUrl);

        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons);
        }
    }

    //fetching sales data and then filtering it
    const fetchSales = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            const filteredSales = data.sales.filter(sale => sale.salesperson.employee_id === parseInt(salesperson));
            setSales(filteredSales)
        }
    }

    useEffect (() => {;
        fetchSales();
    }, [salesperson]);

    useEffect (() => {
        fetchSalesperson();
    }, []);

    function handleSalespersonChange(e) {
        const value = e.target.value;
        setSalesperson(value);
    }

    return (
        <>
        <div className="row">
                <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
                <Link to="/salespeople" className="btn btn-primary btn-lg px-4 gap-3">Salespeople</Link>
                </div>
            <div className="shadow p-4 mt-4">
                <h1>Choose a Salesperson</h1>
                    <div className="mb-3">
                        <select onChange={handleSalespersonChange} value={salesperson} required className="form-select" id="salesperson">
                            <option value="">Choose a Salesperson</option>
                            {salespersons.map(s => {
                            return (
                                <option key={s.employee_id} value={s.employee_id}>
                                    {s.first_name} {s.last_name}
                                </option>
                            )
                            })}
                        </select>
                    </div>
            </div>
        </div>

            <>
            <div>
            <table className="table table-striped">
            <thead>
            <tr>
                <th>Salesperson Employee ID</th>
                <th>Salesperson Name</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            {sales.map((s) => {
                return (
                <tr key={s.id}>
                    <td>{ s.salesperson.employee_id }</td>
                    <td>{ s.salesperson.first_name } { s.salesperson.last_name }</td>
                    <td>{ s.customer.first_name } { s.customer.last_name }</td>
                    <td>{ s.automobile.import_vin }</td>
                    <td>${ s.price }</td>
                </tr>
                );
            })}
            </tbody>
            </table>
            </div>
            </>
        </>
    )
}

export default SalespersonHistory
