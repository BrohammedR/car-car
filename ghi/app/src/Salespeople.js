import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function Salespeople() {

    const [salespersons, setSalespeople] = useState([]);


    //fetch the salespeople data
    const fetchSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/"

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons)
        }
    }
    useEffect(() => {
        fetchSalespeople();
    }, []);

    return (
        <>
        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/salespeople/create" className="btn btn-primary btn-lg px-4 gap-3">Add a Salesperson</Link>
            </div>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Employee ID</th>
            </tr>
            </thead>
            <tbody>
            {salespersons.map((s) => {
                return (
                <tr key={s.employee_id}>
                    <td>{ s.first_name }</td>
                    <td>{ s.last_name }</td>
                    <td>{ s.employee_id }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
    )
}

export default Salespeople
