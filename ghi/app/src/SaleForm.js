import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SaleForm() {
	const navigate = useNavigate();
	const [autos, setAutos] = useState([]);
	const [vin, setVin] = useState('');

	const [salespersons, setSalespersons] = useState([]);
	const [salesperson, setSalesperson] = useState('');

	const [customers, setCustomers] = useState([]);
	const [customer, setCustomer] = useState('');

	const [price, setPrice] = useState('');
	const [successAlert, setSuccessAlert] = useState(false);

	function handleVinChange(e) {
		const value = e.target.value;
		setVin(value);
	}

	function handleSalespersonChange(e) {
		const value = e.target.value;
		setSalesperson(value);
	}

	function handleCustomerChange(e) {
		const value = e.target.value;
		setCustomer(value);
	}
	function handlePriceChange(e) {
		const value = e.target.value;
		setPrice(value);
	}

	//this is for fetching data to populate VIN dropdown menu AND filter to unsold only
	const fetchVins = async () => {
		const vinsUrl = 'http://localhost:8100/api/automobiles/';
		const response = await fetch(vinsUrl);

		if (response.ok) {
			const data = await response.json();
			const unsoldAutos = data.autos.filter((auto) => auto.sold !== true);
			setAutos(unsoldAutos);
		}
	};

	//fetch salesperson data
	const fetchSalesperson = async () => {
		const salespersonUrl = 'http://localhost:8090/api/salespeople/';
		const response = await fetch(salespersonUrl);

		if (response.ok) {
			const data = await response.json();
			setSalespersons(data.salespersons);
		}
	};

	//fetch customer data
	const fetchCustomers = async () => {
		const customerUrl = 'http://localhost:8090/api/customers/';
		const response = await fetch(customerUrl);

		if (response.ok) {
			const data = await response.json();
			setCustomers(data.customers);
		}
	};

	useEffect(() => {
		fetchVins();
		fetchSalesperson();
		fetchCustomers();
	}, []);

	//what to do on subnmit
	const handleSubmit = async (e) => {
		e.preventDefault();
		const data = {
			automobile: vin,
			salesperson: salesperson,
			customer: customer,
			price: price,
		};
		//changing sold status to true
		const autoUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`;
		const soldConfig = {
			method: 'PUT',
			body: JSON.stringify({ sold: true }),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		const soldResponse = await fetch(autoUrl, soldConfig);

		if (soldResponse.ok) {
			const data = await soldResponse.json();
		}

		const saleUrl = 'http://localhost:8090/api/sales/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		fetch(saleUrl, fetchConfig)
			.then((response) => response.json())
			.then(() => {
				setSuccessAlert(true);
				const alertTimeout = setTimeout(() => {
					setSuccessAlert(false);
				}, 3000);
				const navigateTime = setTimeout(() => {
					navigate('/sales');
				}, 1000);
				return () => {
					clearTimeout(alertTimeout);
				};
			});
	};

	return (
		<div>
			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Sale Archived!
				</div>
			)}
			<div className="row">
				<div className="shadow p-4 mt-4">
					<h1>Create a new Sale</h1>
					<form onSubmit={handleSubmit} id="create-automobile-form">
						<div className="mb-3">
							<select
								onChange={handleVinChange}
								value={vin}
								required
								className="form-select"
								id="vin"
							>
								<option value="">Choose a VIN</option>
								{autos.map((auto) => {
									return (
										<option key={auto.id} value={auto.vin}>
											{auto.vin}
										</option>
									);
								})}
							</select>
						</div>
						<div className="mb-3">
							<select
								onChange={handleSalespersonChange}
								value={salesperson}
								required
								className="form-select"
								id="salesperson"
							>
								<option value="">Choose a Salesperson</option>
								{salespersons.map((s) => {
									return (
										<option key={s.employee_id} value={s.employee_id}>
											{s.first_name} {s.last_name}
										</option>
									);
								})}
							</select>
						</div>
						<div>
							<select
								onChange={handleCustomerChange}
								value={customer}
								required
								className="form-select"
								id="customer"
							>
								<option value="">Choose a Customer</option>
								{customers.map((c) => {
									return (
										<option key={c.id} value={c.id}>
											{c.first_name} {c.last_name}
										</option>
									);
								})}
							</select>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handlePriceChange}
								value={price}
								placeholder="price"
								required
								type="text"
								id="price"
								className="form-control"
							/>
							<label htmlFor="price">Price</label>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
					<div>
						<p></p>
					</div>
					<div>
						<p>
							Note: It may take up to 60 seconds after creating an automobile to
							successfully sell it!
						</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default SaleForm;
