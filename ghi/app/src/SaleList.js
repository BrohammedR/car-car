import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function Salelist() {
    const [sales, setSales] = useState([]);

    const handleDelete = async (s) => {
        const saleUrl = `http://localhost:8090/api/sales/${s.id}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(saleUrl, fetchConfig)
        if(response.ok) {
            fetchSales();
        }
    }

    const fetchSales = async () => {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

    useEffect(() => {
        fetchSales();
    }, [])


    return (
        <>
        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/sales/create" className="btn btn-primary btn-lg px-4 gap-3">Add a Sale</Link>
            </div>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Salesperson Employee ID</th>
                <th>Salesperson Name</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            {sales.map((s) => {
                return (
                <tr key={s.id}>
                    <td>{ s.salesperson.employee_id }</td>
                    <td>{ s.salesperson.first_name } { s.salesperson.last_name }</td>
                    <td>{ s.customer.first_name } { s.customer.last_name }</td>
                    <td>{ s.automobile.import_vin }</td>
                    <td>${ s.price }</td>
                    <td>
                        <button onClick={() => handleDelete(s)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
    )
}

export default Salelist
